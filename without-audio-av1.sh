# GitLab supports av1
# GitHub supports vp9
# Telegram supports it

ffmpeg \
	-f x11grab \
	-video_size 1920x1200 -i :0+0,2640 \
	-c:v libsvtav1 -preset 6 \
	~/record/out/$(date +%Y-%b-%d%a--%H-%M-%S | tr A-Z a-z).webm
