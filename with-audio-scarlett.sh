ffmpeg -f x11grab -video_size 1920x1200 -i :0+0,2640 \
	-f pulse -i alsa_output.usb-GN_Netcom_A_S_Jabra_EVOLVE_LINK_00113735E82E0A-00.analog-stereo.monitor \
	-f pulse -i alsa_input.usb-Focusrite_Scarlett_Solo_USB_Y7D1J3F0A66336-00.HiFi__Mic1__source \
	-filter_complex "amerge" -ac 1 \
	-vf setpts=N/FR/TB \
	-c:v libsvtav1 -preset 6 \
	-c:a libopus \
	~/record/out/$(date +%Y-%b-%d%a--%H-%M-%S | tr A-Z a-z).webm


# Press q to finish the recording.
# Devices from `pacmd list-sources | grep -e 'name:' -e 'index:'`
# https://trac.ffmpeg.org/wiki/Encode/AV1
# libsvtav1 -preset: lower number - smaller file size
#
# # setpts=N/FR/TB
# to be able to pause by Ctrl-Z, see https://stackoverflow.com/a/61692055/1879101
