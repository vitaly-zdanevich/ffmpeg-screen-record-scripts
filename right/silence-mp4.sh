ffmpeg \
	-f x11grab \
	-video_size 1920x1200 \
	-i :0.0+1200 \
	~/record/out/$(date +%Y-%b-%d%a--%H-%M-%S | tr A-Z a-z).mp4


# Press q to finish the recording.
# Devices from `pacmd list-sources | grep -e 'name:' -e 'index:'`
# https://trac.ffmpeg.org/wiki/Encode/AV1
