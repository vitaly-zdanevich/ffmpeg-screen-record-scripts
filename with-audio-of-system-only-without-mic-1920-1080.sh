ffmpeg -f x11grab \
	-framerate 60 \
	-video_size 1920x1080 \
	-i :0+0,2700 \
	-f pulse -i alsa_output.usb-GN_Netcom_A_S_Jabra_EVOLVE_LINK_00113735E82E0A-00.analog-stereo.monitor \
	-c:v libsvtav1 -preset 7 \
	-c:a libopus \
	~/record/out/$(date +%Y-%b-%d%a--%H-%M-%S | tr A-Z a-z).webm


# Press q to finish the recording.
# Devices from `pacmd list-sources | grep -e 'name:' -e 'index:'`
# https://trac.ffmpeg.org/wiki/Encode/AV1
# libsvtav1 -preset: lower number - smaller file size

# Forked this to record Armies of Exigo, that runs in 1920x1080
