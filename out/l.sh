while read i; do
	echo "$i" "$(ffprobe -show_entries format=duration -v quiet -of csv="p=0" -sexagesimal "$i")";
done < <(find . -type f \( -iname \*.webm \) -exec ls -t {} +)
