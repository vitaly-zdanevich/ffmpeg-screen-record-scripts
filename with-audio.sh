ffmpeg -f x11grab -video_size 1920x1200 -i :0+0,2640 \
	-f pulse -i alsa_output.usb-GN_Netcom_A_S_Jabra_EVOLVE_LINK_00113735E82E0A-00.analog-stereo.monitor \
	-f pulse -i alsa_input.usb-GN_Netcom_A_S_Jabra_EVOLVE_LINK_00113735E82E0A-00.mono-fallback \
	-filter_complex "amerge" -ac 1 \
	-c:v libsvtav1 -preset 5 \
	-c:a libopus \
	~/record/out/$(date +%Y-%b-%d%a--%H-%M-%S | tr A-Z a-z).webm


# Press q to finish the recording.
# Devices from `pacmd list-sources | grep -e 'name:' -e 'index:'`
# https://trac.ffmpeg.org/wiki/Encode/AV1
# libsvtav1 -preset: lower number - smaller file size
